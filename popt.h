/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef ANDROID_POPT_H
#define ANDROID_POPT_H

/*
 * popt has been deprecated for some time, and is replaced by GNOME's glib
 * option parser. Instead of pulling in either of those dependencies, this
 * stub implements just enough of popt to get things working.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

#define POPT_ARG_NONE		 0U
#define POPT_ARG_STRING		 1U
#define POPT_ARG_INT		 2U
#define POPT_ARG_LONG            3U
#define POPT_ARG_CALLBACK        POPT_ARG_NONE
#define POPT_ARG_INCLUDE_TABLE   POPT_ARG_NONE

#define POPT_ERROR_BADNUMBER     -5

#define POPT_BADOPTION_NOALIAS   0

#define POPT_AUTOHELP

#pragma pack(push)
#pragma pack(0)

struct poptOption {
    const char *longName;
    char shortName;
    unsigned int argInfo;
    void *arg;
    int val;
    const char *descrip;
    const char *argDescrip;
};

#define POPT_TABLEEND { NULL, 0, 0, NULL, 0, NULL, NULL }

struct _poptContext {
    int argc;
    const char **argv;
    const struct poptOption *options;
    struct option *long_options;
    const char *otherHelp;
};

typedef struct _poptContext *poptContext;

#pragma pack(pop)

poptContext poptGetContext(const char *name, int argc, const char **argv,
        const struct poptOption *options, unsigned int flags);
poptContext poptFreeContext(poptContext con);

static inline void poptResetContext(poptContext con __unused) {
    optind = 1;
}


static inline void poptSetOtherOptionHelp(poptContext con, const char *text) {
    con->otherHelp = text;
}

#define poptPrintHelp poptPrintUsage
void poptPrintUsage(poptContext con, FILE *fp, int flags);

int poptGetNextOpt(poptContext con);
static inline const char *poptGetArg(poptContext con) {
    return con->argv[optind++];
}
static inline const char **poptGetArgs(poptContext con) {
    return &con->argv[optind];
}
static inline const char* poptGetOptArg(poptContext con) {
    return strdup(poptGetArg(con));
}

enum poptCallbackReason { INVALID = -1 };

static inline const char *poptGetInvocationName(poptContext con) {
    return con->argv[0];
}

static inline const char *poptStrerror(const int error __unused) {
    return "Unknown";
}

static inline const char *poptBadOption(poptContext con __unused,
                                        int flags __unused) {
    return "Unknown";
}

__END_DECLS

#endif
